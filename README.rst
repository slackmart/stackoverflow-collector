Scouting Stackexchange
----------------------

Running project tests
---------------------

::

   $ python3 -m venv /tmp/se_venv
   $ . /tmp/se_venv/bin/activate
   (/tmp/se_venv) $ python3 setup.py develop
   (/tmp/se_venv) $ python3 setup.py test

For lambda
----------

::
   
   $ pip3 install ./ --update --target ./
