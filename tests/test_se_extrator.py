from unittest.mock import patch

from .fixtures import *
from se_extractor import extract


# https://docs.pytest.org/en/latest/builtin.html?highlight=fixtures

@patch('se_extractor.build_url')
@patch('se_extractor.get_requests_session')
def test_data_extraction(
        session_mock, buildurl_mock, fake_url, session, date_params):
    session_mock.return_value = session
    buildurl_mock.return_value = fake_url
    fdate, tdate = date_params

    data = extract(977593, 526189, fromdate=fdate, todate=tdate)

    assert data == [], f'Hey {data}'
    buildurl_mock.assert_called_once()
    session_mock.assert_called_once()
