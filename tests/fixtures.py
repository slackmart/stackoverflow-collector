from pathlib import Path
from datetime import datetime, timezone, timedelta

from requests import Session
from requests_file import FileAdapter
from pytest import fixture


@fixture(scope='module')
def fake_url():
    answers_path = Path.cwd() / Path('tests/input/answers.json')
    yield f'file:///{answers_path}'


@fixture(scope='module')
def session():
    session = Session()
    session.mount('file://', FileAdapter())
    yield session


@fixture(scope='module')
def date_params():
    today = datetime.now()
    yesterday = today - timedelta(days=1)

    from_date = int(yesterday.replace(tzinfo=timezone.utc).timestamp())
    to_date = int(today.replace(tzinfo=timezone.utc).timestamp())
    yield from_date, to_date
