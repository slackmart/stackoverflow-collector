from se_extractor import extract


def handler(event, context):
    ids = event.get('ids').split(',')
    if not ids:
        return {
            'error_message': f"Users' ids are expected as a comma separated string"
        }
    print('Processing', event)
    fromdate, todate = event.get('fromdate'), event.get('todate')
    page_size = event.get('page_size')
    data = extract(*ids, fromdate=fromdate, todate=todate, page_size=page_size, limit=1)
    print('Collected data', data)
    return {
        'result': data
    }

if __name__ == '__main__':
    ev = {
        "page_size": 1, "ids": "977593, 526189",
        'fromdate': 1528374082, 'todate': 1551649578
    }
    handler(ev, None)
