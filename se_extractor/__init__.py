import logging
import urllib

import requests

from .settings import API_URL, URI, PARAMS as DEFAULT_PARAMS

log = logging.getLogger('stackexchange-extractor')


def join_tags_with_comma(item):
    item['tags'] = ','.join(item['tags'])
    return item


def build_url(*uids):
    """
    >>> build_url(33333)
    'https://api.stackexchange.com/2.2/users/33333/answers'
    >>> build_url(33333, 55555)
    'https://api.stackexchange.com/2.2/users/33333%3B55555/answers'
    """
    uri = URI.format(urllib.parse.quote(';'.join(map(str, uids))))
    return '{}/{}'.format(API_URL, uri)#f'{API_URL}/{uri}'

def get_requests_session():
    return requests.Session()


def get_params(**kwargs):
    """
    >>> get_params(todate='999299991', fromdate='999299191',
    ...     site='serverfault', sort='votes') # doctest: +ELLIPSIS
    {'fromdate': '999299191', 'todate': '999299991', 'page_number': 1, ..., 'sort': 'votes', ..., 'site': 'serverfault', ...}
    >>> get_params(todate='199299191', fromdate='299299191') # doctest: +ELLIPSIS
    {..., 'sort': 'activity', 'order': 'desc', 'site': 'stackoverflow',...}
    >>> get_params(order='desc')
    {}
    """
    if 'fromdate' not in kwargs or 'todate' not in kwargs:
        log.warning('Either fromdate and/or todate were not provided %s', kwargs)
        return {}

    return {
        'fromdate': kwargs.get('fromdate'),
        'todate': kwargs.get('todate'),
        'page_number': kwargs.get('page', 1),
        'page_size': kwargs.get('page_size', 100),
        'sort': kwargs.get('sort', DEFAULT_PARAMS['sort']),
        'order': kwargs.get('order', DEFAULT_PARAMS['order']),
        'site': kwargs.get('site', DEFAULT_PARAMS['site']),
        'filter': kwargs.get('filter', DEFAULT_PARAMS['filter']),
    }


def extract(*uids, **kwargs):
    # FIXME: Conside whether to deal with throttle or not
    #        https://api.stackexchange.com/docs/throttle
    #import pdb; pdb.set_trace()
    url = build_url(*uids)
    log.info('Url: %s', url)

    limit = kwargs.pop('limit', 2)

    params = get_params(**kwargs)
    session = get_requests_session()

    data_list = []
    has_more = True
    while limit and has_more and params:
        response = session.get(url, params=params)
        if response.ok:
            data_json = response.json()
            has_more = data_json['has_more']

            log.info('>>> %s', data_json)
            data_list.append(data_json['items'])
            params['page_number'] += 1
            limit -= 1
    return data_list
