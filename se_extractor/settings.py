OPTIONAL_FIELDS = [
    'migrated_from', 'last_edit_date', 'accepted_answer_id', 'closed_date',
    'closed_reason', 'bounty_closes_date', 'bounty_amount', 'protected_date'
]
API_URL = 'https://api.stackexchange.com/2.2'
URI = 'users/{}/answers'
PARAMS = {
    'order': 'desc',
    'sort': 'activity',
    'site': 'stackoverflow',
    'filter': '!.Fjtn-zdOYbxluiRzUZ.OR-2kd5IT'
}
QUESTIONS_URL = API_URL + 'questions?fromdate={}&to_date={}&pagesize={}&page={}&site=stackoverflow'
CSV_FOLDER = 'data'
CSV_PREFIX = 'data-{}.csv'
