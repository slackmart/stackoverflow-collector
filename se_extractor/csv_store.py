from csv import DictWriter
from pathlib import Path

from .settings import CSV_FOLDER, CSV_PREFIX, OPTIONAL_FIELDS


def to_csv(data, yesterday):
    csv_directory = Path(os.path.dirname(os.path.abspath(__file__))).parent / CSV_FOLDER
    if not csv_directory.exists():
        print(f'{csv_directory} does not exit.. creating')
        csv_directory.mkdir()
    fields = list(data[0][0].keys())
    file_name = csv_directory / CSV_PREFIX.format(yesterday.date())
    with file_name.open('w', encoding='utf-8', newline='') as f:
        dict_writer = DictWriter(f, fieldnames=fields + OPTIONAL_FIELDS)
        dict_writer.writeheader()
        dict_writer.writerows(item for page in data for item in page)
