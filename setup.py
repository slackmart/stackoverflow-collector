# -*- coding: utf-8 -*-

from pathlib import Path
from setuptools import setup, find_packages


def parse_requirements(filename):
    requirements = (Path(__file__).parent / filename).read_text().strip().split('\n')
    return list(map(str.strip, requirements))


setup(
    version='v0.0.1',
    name='se-extractor',
    packages=find_packages(),
    url='https://gitlab.com/slackmart/stackoverflow-collector',
    keywords=['api', 'stackoverflow', 'stackexchange'],
    install_requires=parse_requirements('requirements.txt'),
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'requests-file'],
    license='MIT'
)
